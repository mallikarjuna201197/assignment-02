import org.junit.Assert;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;





public class Assignment2 {
	
		
		WebDriver dr;
		
		 @Before
		    public void startApp()
		{
			 System.setProperty("webdriver.chrome.driver", "D:\\ALM image\\Sel\\chromedriver.exe");
		     dr=new ChromeDriver();
			 dr.get("http://shop.demoqa.com");
		}
		 
		 @After
		 public void CloseApp()
		 {
			 dr.close();
		 }
		
		/* @Test
		 public void screenTitle()
		 {
			 String actualTitle = dr.getTitle();
			 String expectedTitle = "SHOP.TOOLS";
			Assert.assertEquals(expectedTitle,actualTitle);
			
			 
		 }*/
		 
		 @Test
		 public void link()
		 {
			 String url = dr.getCurrentUrl();
			 System.out.println(url);
			 Assert.assertEquals("http://shop.demoqa.com", url);
		 }
		 
	}

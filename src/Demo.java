import java.util.ArrayList;

public class Demo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr= {-1,-2,-3,4};
		int sum=0,max=0;
		ArrayList<Integer> list=new ArrayList<Integer>();
		for(int i=0;i<(1<<arr.length);i++)
		{
			list.clear();
			for(int j=0;j<arr.length;j++) 
			{
				if((i&(1<<j))>0) {
					list.add(arr[j]);
			}
		}
			sum=0;
			for(int k=0;k<list.size();k++) 
			{
				System.out.print(list.get(k)+",");
				sum=sum+(list.get(k)*(k+1));
			}
			  System.out.print("="+sum);
			if(sum>max)
			{
				max=sum;
			}
			System.out.println();
		}
		System.out.println(max);
	}

}
